﻿using System;
using System.Linq;
using Calculation.Interfaces;

namespace Calculation.Implementation
{
	public class Calculator : BaseCalculator, ICalculator, ILogger
	{
        private readonly ILogger _logger;

		public Calculator(ILogger logger)
		{
            _logger = logger;
		}

		public int Sum(params int[] numbers)
		{
            _logger.Trace($"Invocated method: {nameof(Sum)} with parameters: {string.Join(", ", numbers)}");

            int sum = 0;
            try
            {
                sum = SafeSum(numbers);

                _logger.Info($"Invocated method: {nameof(Sum)} with parameters: {string.Join(", ", numbers)} with the result: {sum}");               
            }
			catch (OverflowException ex)
            {               
                _logger.Error(ex);
                throw;
            }
            finally
            {
                _logger.Trace($"End of method: {nameof(Sum)}");
            }
            
            return sum;
        }

		public int Sub(int a, int b)
		{
            try
            {
				return SafeSub(a, b);
			}
            catch (OverflowException ex)
            {
                throw new OverflowException(ex.Message, ex);
            }
		}

		public int Multiply(params int[] numbers)
		{
			if (!numbers.Any())
				return 0;
            try
            {
				return SafeMultiply(numbers);
			}
            catch (OverflowException ex)
            {
                throw new InvalidOperationException("Invalid operation", ex);
            }	
		}

		public int Div(int a, int b)
		{
            try
            {
				return a / b;
			}
            catch
            {
				throw new InvalidOperationException();
            }
		}

        public void Error(Exception ex) => NLog.LogManager.GetCurrentClassLogger().Error(ex);

        public void Info(string message) => NLog.LogManager.GetCurrentClassLogger().Info(message);

        public void Trace(string message) => NLog.LogManager.GetCurrentClassLogger().Trace(message);
    }
}